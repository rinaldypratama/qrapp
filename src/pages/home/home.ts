import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  qrData:string = null;
  createdCode = null;
  scannedCode = null;
  form = "http://";

  constructor(public barcodeScanner : BarcodeScanner) { }

  createCode(){
  	if(this.qrData.includes("http") || this.qrData.includes("https")){
  		console.log('1');
  		this.createdCode = this.qrData;
  	}
  	else{
  		console.log('2');
  		this.createdCode = this.form+this.qrData;
  	}
  }

  scanCode(){
  	this.barcodeScanner.scan().then(barcodeData => {
  		this.scannedCode = barcodeData.text;
  		console.log(barcodeData);
  	}, (err) => {
  		console.log('Error: ', err);
  	});
  }

}
